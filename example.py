from pprint import pprint
import sqlite3

conn = sqlite3.connect("events.sqlite3")
cur = conn.cursor()

print("events for name 4")
cur.execute("SELECT * FROM event WHERE name=4")
pprint(cur.fetchall())
print()

print("average attitude for all names")
cur.execute("SELECT name, AVG(attitude) FROM event GROUP BY name")
pprint(cur.fetchall())
