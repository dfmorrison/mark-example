SQLite example for Mark
=======================

This is a trivial example of accessing a CP100 events database from Python.

This is Python 3 code. I’ve only tested it in Python 3.9, but it’ll probably work in any recent Python 3.

All it does is opens the database events.sqlite3 in this directory, and prints two things

* all the events associated with name 4

* the averages of all the non-null attitutude values over all events for each name

To run it just do

    python example.py

To learn more about the Python sqlite3 module see
the [DB-API 2.0 interface for SQLite databases documentation](https://docs.python.org/3/library/sqlite3.html).
